__author__ = 'mikhail'

import json

def read_from_file(filename):
    try:
        with open(filename) as f:
            lines = f.readlines()
            data = [line.split() for line in lines]
            for i, sample in enumerate(data):
                if len(sample) != 4:
                    print('Warning! Incorrect sample length!')
                else:
                    day, hour, clicks, count = sample
                    data[i] = [int(day), int(hour), int(clicks), float(count)]
        return data
    except FileNotFoundError:
        print("Error! File doesn't exists.")
        return []

