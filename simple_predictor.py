__author__ = 'mikhail'

class SimplePredictor(object):
    def __init__(self):
        self.stats = {(day, hour): 0 for day in range(1, 8) for hour in range(0, 24)}

    def fit(self, data):
        samples = len(data)
        samples_count = {(day, hour): 0 for day in range(1, 8) for hour in range(0, 24)}
        for day, hour, clicks, count in data:
            if clicks != 0:
                self.stats[(day, hour)] += count / clicks
                samples_count[(day, hour)] += 1
        for key in self.stats:
            if samples_count[key]:
                self.stats[key] /= samples_count[key]

    def predict(self, day, hour, clicks):
        return self.stats[(day, hour)] * clicks
