Интерфейс приложения.

Модуль simple_predictor:
    класс SimplePredictor:
        метод fit(data) - выполнить анализ собранной статистики data(в формате, который возвращает функция read_from_file из модуля utils) и настроить внутренние параметры прогнозирования
        метод predict(day, hour, clicks) - принимает на вход номер дня, часа, количество кликов и возвращает прогноз на количество заказов

Модуль utils:
    функция read_from_file(filename) - зачитывает статистику из файла и возвращает список из списков вида [day, hour, clicks, count]

Модуль main:
    функция process_request(request) - принимает на вход строку запроса в формате json, возвращает результат выполнения запроса в виде строки в формате json.
    
Пример использования в модуле main:

test_request = '{"model": "samples_10K.txt", "query": "predict", \ # тестовый запрос
                        "data": {"day": 5, "hour": 2, "clicks": 263}}'

if __name__ == '__main__':
    print("Test request: ", test_request)            # текст запроса
    print("Answer: ", process_request(test_request)) # и результат его выполнения
