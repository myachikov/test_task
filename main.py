__author__ = 'mikhail'

import json

import utils
from simple_predictor import SimplePredictor

def process_request(request):
    request = json.loads(request)
    try:
        data = utils.read_from_file(request['model'])
        if request['query'] == 'predict':
            sp = SimplePredictor()
            sp.fit(data)
        return json.dumps({'count': sp.predict(**request['data'])})
    except KeyError:
        print('Key error!')
        return json.dumps({"error": 1})

test_request = '{"model": "samples_10K.txt", "query": "predict", \
                        "data": {"day": 5, "hour": 2, "clicks": 263}}'

if __name__ == '__main__':
    print("Test request: ", test_request)
    print("Answer: ", process_request(test_request))
    print("Mean squared error test...")
    data = utils.read_from_file('samples_10K.txt')
    sp = SimplePredictor()
    sp.fit(data)
    errors = [(count - sp.predict(day, hour, clicks)) for day, hour, clicks, count in data]
    MSE = sum(errors) / len(errors)
    print("MSE: ", MSE)
